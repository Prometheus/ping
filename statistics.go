package ping

import (
	"math"
	"net"
	"sync"
	"time"
)

// Statistics represent the stats of a currently running or finished
// pinger operation.
// All methods on *Statistics are thread safe, but field access is not.
type Statistics struct {
	sync.Mutex

	// Received is the number of packets received.
	Received int

	// Sent is the number of packets sent.
	Sent int

	// IPAddr is the address of the host being pinged.
	IPAddr *net.IPAddr

	// Host is the hostname of the host being pinged.
	Host string

	// Source is the source hostname or IP pings are sent from.
	Source string

	// Min is the minimum round-trip time.
	Min time.Duration

	// Max is the maximum round-trip time.
	Max time.Duration

	// Mean is the average round-trip time.
	Mean time.Duration

	// Sum is the of all round-trip times.
	Sum time.Duration

	// State for calculation of variance
	sumSq, mean float64
}

// NewStatistics returns an initialised Statistics struct pointer
func NewStatistics(addr string, ipAddr *net.IPAddr) *Statistics {
	return &Statistics{
		Host:   addr,
		IPAddr: ipAddr,
		Min:    math.MaxInt32,
	}
}

// Increment the sent count
func (s *Statistics) IncSent() {
	s.Lock()
	defer s.Unlock()
	s.Sent++
}

// Add updates the statistics with this new value
func (s *Statistics) Add(rtt time.Duration) {
	s.Lock()
	defer s.Unlock()

	s.Received++
	s.Sum += rtt

	if rtt > s.Max {
		s.Max = rtt
	}
	if rtt < s.Min {
		s.Min = rtt
	}

	// Use Welford's algorithm for variance
	frtt := float64(rtt)
	newMean := float64(s.Sum) / float64(s.Received)
	if s.Received == 1 {
		s.mean = frtt
	} else {
		s.sumSq += (frtt - s.mean) * (frtt - newMean)
		if s.sumSq < 0 {
			s.sumSq = 0
		}
	}
	s.mean = newMean
	s.Mean = time.Duration(s.mean)
}

// PacketLoss returns the packet loss ratio.
func (s *Statistics) PacketLoss() float64 {
	s.Lock()
	defer s.Unlock()

	if s.Sent == 0 {
		return math.NaN()
	}
	return 1 - float64(s.Received)/float64(s.Sent)
}

// variance returns the variance of the round-trip times sent via the pinger as a float64
func (s *Statistics) variance() float64 {
	s.Lock()
	defer s.Unlock()

	if s.Received < 2 {
		return 0
	}
	return s.sumSq / float64(s.Received-1)
}

// Variance returns the variance of the round-trip times sent via the pinger.
func (s *Statistics) Variance() time.Duration {
	return time.Duration(s.variance())
}

// StandardDeviation returns the standard deviation of the round-trip times
// sent via this pinger.
func (s *Statistics) StandardDeviation() time.Duration {
	return time.Duration(math.Sqrt(s.variance()))
}

// Durations returns the durations values of the statistics.
func (s *Statistics) Durations() (min, mean, max time.Duration) {
	s.Lock()
	defer s.Unlock()

	return s.Min, s.Mean, s.Max
}

// Counters returns the counter values of the statistics.
func (s *Statistics) Counters() (recv, sent int) {
	s.Lock()
	defer s.Unlock()

	return s.Received, s.Sent
}
