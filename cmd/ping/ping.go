package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"

	"git.slxh.eu/prometheus/ping"
)

func main() {
	// Create an unconfigured Pinger
	pinger := new(ping.Pinger)

	// Let the flags configure the Pinger
	flag.DurationVar(&pinger.TimeLimit, "t", ping.DefaultTimeLimit, "Time limit")
	flag.DurationVar(&pinger.Interval, "i", ping.DefaultInterval, "Interval")
	flag.IntVar(&pinger.Size, "s", ping.DefaultSize, "Number of bytes to send")
	flag.IntVar(&pinger.PacketLimit, "c", 0, "Number of packets to send")
	flag.Parse()

	// Show usage and exit if no argument given
	if flag.NArg() == 0 {
		flag.Usage()
		os.Exit(1)
	}

	// Set the host of the pinger
	err := pinger.SetHost(flag.Arg(0))
	if err != nil {
		fmt.Printf("%s: %s", os.Args[0], err.Error())
		return
	}

	// Listen for an interrupt (ctrl+c)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			pinger.Stop()
		}
	}()

	// Hook to call when receiving a message
	pinger.OnReceive = func(pkt *ping.Packet) {
		fmt.Printf("%d bytes from %s: icmp_seq=%d time=%v\n",
			pkt.Size, pkt.IPAddr, pkt.Seq, pkt.RTT)
	}

	// Hook to call after the ping is finished
	pinger.OnFinish = func(stats *ping.Statistics) {
		fmt.Printf("\n--- %s ping statistics ---\n", stats.Host)
		fmt.Printf("%d packets transmitted, %d packets received, %v%% packet loss\n",
			stats.Sent, stats.Received, 100*stats.PacketLoss())
		fmt.Printf("round-trip min/avg/max/stddev = %v/%v/%v/%v\n",
			stats.Min, stats.Mean, stats.Max, stats.StandardDeviation())
	}

	// Run the Pinger
	fmt.Printf("PING %s (%s): %v data bytes\n", pinger.Host, pinger.IPAddr, pinger.Size)
	err = pinger.Run()
	if err != nil {
		fmt.Printf("Error: %s", err)
	}
}
