# ping
[![GoDoc][badge]][godoc]

A simple ping library that aims to make the retrieval of ping-statistics easy and manageable.
This project is a fork of [go-ping][], with the following major changes:

- Unprivileged (UDP) pinging has been removed.
- Almost all fields have been renamed.
- Statistics are calculated on the fly, and no history of round-trip times is stored.
- Main initialisation is not done in the `NewPinger` function, but when `Run` is called.
- The given `Size` is respected: when the timestamp cannot be stored in the packet,
  it is kept in the `Pinger`. The data is also not encoded as JSON.

Also see [the documentation][godoc]

## Installation

Download & install using:

```
go get git.slxh.eu/prometheus/ping/...
```

## Usage

An full implementation matching the Unix `ping` command is included in `cmd/ping`.

The following example sends three packets to Google:

```go
pinger, err := ping.NewPinger("www.google.com")
if err != nil {
        panic(err)
}
pinger.PacketLimit = 3
err = pinger.Run()
if err != nil {
	panic(err)
}
stats := pinger.Statistics
```

If the destination address is already available, the `Pinger` can also be initialised directly:

```go
pinger := &ping.Pinger{
    IPAddr:    ipAddr, // some *net.IPAddr
    Host:      ipAddr.String(),
    Size:      ping.DefaultSize,
}
```

## Note
On Linux, any binary using this code requires the `cap_net_raw` capability,
this can be set on a binary with the following command:

```
setcap cap_net_raw=+ep $GOPATH/bin/ping
```

See the documentation for [golang.org/x/net/icmp][] for more details.

[badge]: https://godoc.org/git.slxh.eu/prometheus/ping?status.svg
[godoc]: https://godoc.org/git.slxh.eu/prometheus/ping
[go-ping]: https://github.com/sparrc/go-ping
[golang.org/x/net/icmp]: https://godoc.org/golang.org/x/net/icmp
