package ping

import (
	"net"
	"runtime/debug"
	"testing"
)

func TestNewPingerValid(t *testing.T) {
	// Test IPv4-only host
	p, err := NewPinger("ipv4.google.com")
	AssertNoError(t, err)
	AssertEqualStrings(t, "ipv4.google.com", p.Host)
	AssertTrue(t, p.ipv4())

	// Test IPv6-only host
	err = p.SetHost("ipv6.google.com")
	AssertNoError(t, err)
	AssertEqualStrings(t, "ipv6.google.com", p.Host)
	AssertTrue(t, !p.ipv4())

	// Test IPv4 address
	p, err = NewPinger("127.0.0.1")
	AssertNoError(t, err)
	AssertEqualStrings(t, "127.0.0.1", p.Host)
	AssertEqualStrings(t, p.Host, p.IPAddr.String())
	AssertTrue(t, p.ipv4())

	// Test IPv4 address
	p, err = NewPinger("2001:db8::1")
	AssertNoError(t, err)
	AssertEqualStrings(t, "2001:db8::1", p.Host)
	AssertEqualStrings(t, p.Host, p.IPAddr.String())
	AssertTrue(t, !p.ipv4())
}

func TestNewPingerInvalid(t *testing.T) {
	_, err := NewPinger("127.0.0.0.1")
	AssertError(t, err, "127.0.0.0.1")

	_, err = NewPinger("127..0.0.1")
	AssertError(t, err, "127..0.0.1")

	_, err = NewPinger(":::1")
	AssertError(t, err, ":::1")

	_, err = NewPinger("test.invalid")
	AssertError(t, err, "test.invalid")
}

func TestSetIPAddr(t *testing.T) {
	addr, err := net.ResolveIPAddr("ip", "www.google.com")
	if err != nil {
		t.Fatal("Can't resolve www.google.com, can't run tests")
	}

	// Create a localhost pinger
	p, err := NewPinger("localhost")
	AssertNoError(t, err)

	// set IPAddr to google
	p.IPAddr = addr
	AssertEqualStrings(t, addr.String(), p.IPAddr.String())
}

// Test helpers
func AssertNoError(t *testing.T, err error) {
	if err != nil {
		t.Errorf("Expected No Error but got %s, Stack:\n%s",
			err, string(debug.Stack()))
	}
}

func AssertError(t *testing.T, err error, info string) {
	if err == nil {
		t.Errorf("Expected Error but got %s, %s, Stack:\n%s",
			err, info, string(debug.Stack()))
	}
}

func AssertEqualStrings(t *testing.T, expected, actual string) {
	if expected != actual {
		t.Errorf("Expected %s, got %s, Stack:\n%s",
			expected, actual, string(debug.Stack()))
	}
}

func AssertNotEqualStrings(t *testing.T, expected, actual string) {
	if expected == actual {
		t.Errorf("Expected %s, got %s, Stack:\n%s",
			expected, actual, string(debug.Stack()))
	}
}

func AssertTrue(t *testing.T, b bool) {
	if !b {
		t.Errorf("Expected True, got False, Stack:\n%s", string(debug.Stack()))
	}
}

func AssertFalse(t *testing.T, b bool) {
	if b {
		t.Errorf("Expected False, got True, Stack:\n%s", string(debug.Stack()))
	}
}
