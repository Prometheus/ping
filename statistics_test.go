package ping

import (
	"math"
	"testing"
	"time"
)

const failString = "Error testing %s for %s: expected %v, got %v"

var (
	statsTests = map[string]*statsTest{
		"empty": {
			RTTs: []time.Duration{},
			Sent:       0,
			Received:   0,
			PacketLoss: math.NaN(),
			Min:        math.MaxInt32,
			Max:        0,
			Mean:       0,
			Variance:   0,
		},
		"single": {
			RTTs: []time.Duration{1000},
			Sent:       1,
			Received:   1,
			PacketLoss: 0,
			Min:        1000,
			Max:        1000,
			Mean:       1000,
			Variance:   0,
		},
		"perfect": {
			RTTs: []time.Duration{
				1000, 1000, 1000, 1000, 1000,
				1000, 1000, 1000, 1000, 1000,
			},
			Sent:       10,
			Received:   10,
			PacketLoss: 0,
			Min:        1000,
			Max:        1000,
			Mean:       1000,
			Variance:   0,
		},
		"bad": {
			RTTs: []time.Duration{
				10, 1000, 1000, 10000, 1000,
				800, 1000, 40, 100000, 1000,
			},
			Sent:       40,
			Received:   10,
			PacketLoss: 0.75,
			Min:        10,
			Max:        100000,
			Mean:       11585,
			Variance:   973724383,
		},
	}
)

type statsTest struct {
	RTTs                     []time.Duration
	Sent, Received           int
	PacketLoss               float64
	Min, Max, Mean, Variance time.Duration
}

func (t *statsTest) init(name string) *Statistics {
	s := NewStatistics(name, nil)
	s.Sent = t.Sent
	for _, rtt := range t.RTTs {
		s.Add(rtt)
	}
	return s
}

func TestStatistics(t *testing.T) {
	for n, test := range statsTests {
		s := test.init(n)
		if s.Received != test.Received {
			t.Errorf(failString, "received", n, test.Received, s.Received)
		}
		if s.Sent != test.Sent {
			t.Errorf(failString, "sent", n, test.Sent, s.Sent)
		}
		if s.PacketLoss() != test.PacketLoss && math.IsNaN(s.PacketLoss()) != math.IsNaN(test.PacketLoss) {
			t.Errorf(failString, "packet loss", n, test.PacketLoss, s.PacketLoss())
		}
		if s.Min != test.Min {
			t.Errorf(failString, "min", n, test.Min, s.Min)
		}
		if s.Max != test.Max {
			t.Errorf(failString, "max", n, test.Max, s.Max)
		}
		if s.Mean != test.Mean {
			t.Errorf(failString, "mean", n, test.Mean, s.Mean)
		}
		if s.Variance() != test.Variance {
			t.Errorf(failString, "variance", n, test.Variance, s.Variance())
		}
	}
}
