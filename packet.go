package ping

import (
	"net"
	"time"
)

type packet struct {
	bytes []byte
	ts    time.Time
}

// Packet represents a received and processed ICMP echo packet.
type Packet struct {
	// RTT is the round-trip time it took to ping.
	RTT time.Duration

	// IPAddr is the address of the host being pinged.
	IPAddr *net.IPAddr

	// Host is the string address of the host being pinged.
	Host string

	// Source is the source address the ping was sent from.
	Source string

	// NBytes is the number of bytes in the message.
	Size int

	// Seq is the ICMP sequence number.
	Seq int
}
