package ping

import (
	"encoding/binary"
	"time"

	"golang.org/x/net/ipv4"
)

// padding fills a byte slice with padding
func padding(b []byte) {
	for i := range b {
		b[i] = byte(i)
	}
}

func ipv4Payload(b []byte) []byte {
	if len(b) < ipv4.HeaderLen {
		return b
	}
	hdrlen := int(b[0]&0x0f) << 2
	return b[hdrlen:]
}

func unmarshalTime(b []byte) time.Time {
	nsec := int64(binary.BigEndian.Uint64(b))
	return time.Unix(nsec/1000000000, nsec%1000000000)
}

func marshalTime(t time.Time, b []byte) {
	binary.BigEndian.PutUint64(b, uint64(t.UnixNano()))
}
