// Package ping is an ICMP ping library seeking to emulate the unix "ping"
// command.
//
// Here is a very simple example that sends and receives 3 packets:
//
//	pinger, err := ping.NewPinger("www.google.com")
//	if err != nil {
//		panic(err)
//	}
//
//	pinger.PacketLimit = 3
//	pinger.Run() // blocks until finished
//	stats := pinger.Statistics // get send/receive/rtt stats
//
//
// For a full ping example, see "cmd/ping/ping.go".
//
package ping

import (
	"fmt"
	"math"
	"math/rand"
	"net"
	"sync"
	"syscall"
	"time"

	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv4"
	"golang.org/x/net/ipv6"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// Default values for the Pinger
const (
	DefaultSize      = 8
	DefaultInterval  = 1 * time.Second
	DefaultTimeLimit = 100000 * time.Second
)

const (
	protocolICMP     = 1
	protocolIPv6ICMP = 58
	deadline         = time.Millisecond * 100
)

// Pinger represents ICMP packet sender/receiver.
// It is not entirely thread-safe. Troublesome attributes are marked as such.
type Pinger struct {
	// Interval is the wait time between each packet send. Default is 1s.
	Interval time.Duration

	// TimeLimit specifies a timeout before ping exits, regardless of how many
	// packets have been received.
	TimeLimit time.Duration

	// PacketLimit tells pinger to stop after sending (and receiving) PacketLimit echo
	// packets. If this option is not specified, pinger will operate until
	// interrupted.
	PacketLimit int

	// Number of packets sent
	Sent int

	// Number of packets received
	Received int

	// OnReceive is called when Pinger receives and processes a packet
	OnReceive func(*Packet)

	// OnFinish is called when Pinger exits
	OnFinish func(*Statistics)

	// Size of packet being sent
	Size int

	// stop chan bool
	done chan bool

	// Resolved IP Address
	IPAddr *net.IPAddr

	// Host to ping
	Host string

	// Resolved source IP Address
	SourceAddr *net.IPAddr

	// Source host
	Source string

	// Statistics returns the statistics of the pinger. This can be run while the
	// pinger is running or after it is finished. OnFinish uses this to get its
	// finished statistics.
	// Access is thread-safe as long as Statistics is set before calling Run().
	Statistics *Statistics

	// ID of this pinger to separate it from others
	id int

	// The last timestamp used
	lastTimestamp time.Time
}

// NewPinger returns a new Pinger struct pointer with initialized statistics.
// The source address may be an empty string.
func NewPinger(host string) (*Pinger, error) {
	// Resolve the given address
	ipaddr, err := net.ResolveIPAddr("ip", host)
	if err != nil {
		return nil, err
	}

	// Create and initialise the Pinger with sane defaults
	p := &Pinger{
		IPAddr:     ipaddr,
		Host:       host,
		Interval:   DefaultInterval,
		TimeLimit:  DefaultTimeLimit,
		Size:       DefaultSize,
		Statistics: NewStatistics(host, ipaddr),
	}

	return p, nil
}

// SetHost sets the host and the corresponding IP address
func (p *Pinger) SetHost(host string) error {
	ipAddr, err := net.ResolveIPAddr("ip", host)
	if err != nil {
		return err
	}
	p.Host = host
	p.IPAddr = ipAddr
	if p.Statistics != nil {
		p.Statistics.IPAddr = ipAddr
		p.Statistics.Host = host
	}
	return nil
}

// SetSource sets the source address and corresponding IP address.
func (p *Pinger) SetSource(host string) error {
	ipAddr, err := net.ResolveIPAddr("ip", host)
	if err != nil {
		return err
	}
	p.Source = host
	p.SourceAddr = ipAddr
	if p.Statistics != nil {
		p.Statistics.Source = host
	}
	return nil
}

// Run runs the pinger. This is a blocking function that will exit when it's
// done. If PacketLimit or Interval are not specified, it will run continuously until
// it is interrupted.
func (p *Pinger) Run() (err error) {
	var conn *icmp.PacketConn
	var wg sync.WaitGroup
	defer wg.Wait()

	// Create a random ID
	p.id = rand.Intn(math.MaxInt16)

	// Create channel
	if p.done == nil {
		p.done = make(chan bool)
	}

	// Set default interval if none is set
	if p.Interval == 0 {
		p.Interval = DefaultInterval
	}

	// Set default timeout if none is set
	if p.TimeLimit == 0 {
		p.TimeLimit = DefaultTimeLimit
	}

	// Resolve address if none is set
	if p.IPAddr == nil {
		err = p.SetHost(p.Host)
		if err != nil {
			return err
		}
	}

	// Resolve source address if none is set
	if p.SourceAddr == nil {
		err = p.SetSource(p.Source)
		if err != nil {
			return err
		}
	}

	// Initialize statistics if required
	if p.Statistics == nil {
		p.Statistics = NewStatistics(p.Host, p.IPAddr)
		p.Statistics.Source = p.Source
	}

	// Get the correct network protocol
	netProto := "ip6:ipv6-icmp"
	if p.ipv4() {
		netProto = "ip4:icmp"
	}

	// Open the listen socket
	conn, err = icmp.ListenPacket(netProto, p.Source)
	if err != nil {
		return err
	}
	defer conn.Close()

	// Call the finish hook if set
	if p.OnFinish != nil {
		defer p.OnFinish(p.Statistics)
	}

	// Create packet receive channel
	recv := make(chan *packet, 5)

	// Start receiver
	go func() {
		wg.Add(1)
		p.recvICMP(conn, recv)
		wg.Done()
	}()

	// Send the first ping
	err = p.sendICMP(conn)
	if err != nil {
		return err
	}

	// Create timeout/interval tickers
	timeout := time.NewTicker(p.TimeLimit)
	interval := time.NewTicker(p.Interval)
	defer timeout.Stop()
	defer interval.Stop()

	// Ping loop
	for {
		select {
		// We are done
		case <-p.done:
			return
		// Timeout reached
		case <-timeout.C:
			return
		// Next ping
		case <-interval.C:
			if p.PacketLimit > 0 && p.Sent >= p.PacketLimit {
				return
			}
			err = p.sendICMP(conn)
			if err != nil {
				return err
			}
		// Received a response
		case r := <-recv:
			// Check if channel was closed
			if r == nil {
				return
			}
			err := p.processPacket(r)
			if err != nil {
				return err
			}
		}
	}
}

// Stop stops a running Pinger.
func (p *Pinger) Stop() {
	if p.done != nil {
		p.done <- true
	}
}

// recvICMP is the thread receiving ping commands
func (p *Pinger) recvICMP(conn *icmp.PacketConn, recv chan<- *packet) {
	for {
		select {
		case <-p.done:
			close(recv)
			return
		default:
			bytes := make([]byte, 1500)

			// Set the timeout, ignore any errors
			_ = conn.SetReadDeadline(time.Now().Add(deadline))

			// Receive
			n, _, err := conn.ReadFrom(bytes)
			if err != nil {
				// Check if timeout
				if netErr, ok := err.(*net.OpError); ok && netErr.Timeout() {
					continue
				} else {
					return
				}
			}
			recv <- &packet{bytes: bytes[:n], ts: time.Now()}
		}
	}
}

// processPacket processes a received packet
func (p *Pinger) processPacket(recv *packet) error {
	// Set bytes and protocol to the correct value
	bytes := recv.bytes
	proto := protocolIPv6ICMP
	if p.ipv4() {
		bytes = ipv4Payload(recv.bytes)
		proto = protocolICMP
	} else {
		bytes = recv.bytes
		proto = protocolIPv6ICMP
	}

	// Parse message
	m, err := icmp.ParseMessage(proto, bytes)
	if err != nil {
		return err
	}

	// Ignore non-echo replies
	if m.Type != ipv4.ICMPTypeEchoReply && m.Type != ipv6.ICMPTypeEchoReply {
		return nil
	}

	// Interpret body as Echo
	pkt, ok := m.Body.(*icmp.Echo)
	if !ok {
		return fmt.Errorf("invalid ICMP echo reply. Body type: %T, %s", m.Body, m.Body)
	}

	// Check if reply from same ID
	if pkt.ID != p.id {
		return nil
	}

	// Get timestamp from packet if encoded
	ts := p.lastTimestamp
	if len(pkt.Data) >= DefaultSize {
		ts = unmarshalTime(pkt.Data)
	}

	// Create packet to send on
	outPkt := &Packet{
		Size:   len(recv.bytes),
		IPAddr: p.IPAddr,
		Host:   p.Host,
		Source: p.Source,
		Seq:    pkt.Seq,
		RTT:    time.Since(ts),
	}

	// Add the packet to the statistics, and send it to the receive hook
	p.Received++
	p.Statistics.Add(outPkt.RTT)
	if p.OnReceive != nil {
		go p.OnReceive(outPkt)
	}

	return nil
}

// sendICMP sends an ICMP packet on a connection
func (p *Pinger) sendICMP(conn *icmp.PacketConn) error {
	// packet type
	var typ icmp.Type = ipv6.ICMPTypeEchoRequest
	if p.ipv4() {
		typ = ipv4.ICMPTypeEcho
	}

	// Create the message data
	data := make([]byte, p.Size)

	// Add time to message if large enough
	// Store locally otherwise
	if p.Size >= DefaultSize {
		marshalTime(time.Now(), data)
	} else {
		p.lastTimestamp = time.Now()
	}

	// Add padding to message
	if p.Size-DefaultSize > 0 {
		padding(data[DefaultSize:])
	}

	// Create message
	msg := &icmp.Message{
		Type: typ,
		Code: 0,
		Body: &icmp.Echo{
			ID:   p.id,
			Seq:  p.Sent,
			Data: data,
		},
	}

	// Convert to bytes
	bytes, err := msg.Marshal(nil)
	if err != nil {
		return err
	}

	// Send the packet
	for {
		_, err := conn.WriteTo(bytes, p.IPAddr)

		// Retry when ENOBUFS occurs
		if netErr, ok := err.(*net.OpError); ok && netErr.Err == syscall.ENOBUFS {
			continue
		}
		break
	}

	// Update counters
	p.Sent++
	p.Statistics.IncSent()

	return nil
}

// ipv4 returns true if the destination or source address is an IPv4 address
func (p *Pinger) ipv4() bool {
	return p.IPAddr.IP.To4() != nil ||
		p.SourceAddr != nil && p.SourceAddr.IP.To4() != nil
}
